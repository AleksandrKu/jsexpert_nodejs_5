var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var nunjucks = require('nunjucks');

var indexRouter = require('./routes');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use((req, res, next) => {
    let obj = {
        originalUrl: req.originalUrl,
        path: req.path,
        query: req.query,
        params: req.params
    }
    console.log(obj);
    next();
});

app.use((req, res, next) => {
    const startHrTime = process.hrtime();
    res.on("finish", () => {
        setTimeout(() => {
            const diffHrTime = process.hrtime(startHrTime);
            const timeInMs = diffHrTime[0] * 1000 + diffHrTime[1] / 1000000;
            console.log(req.path, timeInMs + "ms");
        }, 1000);
    });
    next();
});
app.use(indexRouter);

nunjucks.configure(path.join(__dirname, 'views'), {
    autoescape: true,
    express: app
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
