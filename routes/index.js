var express = require('express');
var router = express.Router();
const { getList } = require('../controller/get_list');
const { getListItemById } = require('../controller/get_list_item_by_id');

router.get('/items', getList);
router.get('/items/:id', getListItemById);
router.get('*', (req, res) => {
    res.send('No page!!!');
 });
module.exports = router;
