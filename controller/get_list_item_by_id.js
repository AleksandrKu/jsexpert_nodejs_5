exports.getListItemById = (req, res, next) => {
    const items = require('../data/index');
    let isFind = false;
    for (let i = 0; i < items.data.length; i++) {
        if (items.data[i].id == req.params.id) {
            isFind = true;
            res.send(items.data[i]);
        }
    }
    if (!isFind) {
        res.send("No item with id=" + req.params.id);
    }
    res.status(200).end();
    //res.render('index', { title: 'getListItemById' });
};